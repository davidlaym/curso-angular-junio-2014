describe('servicio Personas', function() {

	var servicioPersonas = null;
	var httpBackend = null;

	beforeEach(module('personas'));

    beforeEach(inject(function($injector) {
        httpBackend = $injector.get('$httpBackend');
        servicioPersonas = $injector.get("servicioPersonas");
		
		httpBackend
            .when('GET', '/data/people.json')
            .respond('ok');     
 
      }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('Debe existir servicioPersonas y comprobamos que no sea NULL',function(){
		servicioPersonas.should.not.be.null;
	});

	it('al invocar el servicio se debe hacer el llamado al json', function(){
		httpBackend.expectGET('/data/people.json');
		servicioPersonas.getPersonas();
		httpBackend.flush();		
	});

    
	it('al invocar el servicio se debe retornar datos esperados', function(done){
		httpBackend.expectGET('/data/people.json');
		var persona = servicioPersonas.getPersonas();

		persona.then(function(data){
			data.should.be.equal('ok');
			done();
		});

		httpBackend.flush();	
		//persona.should.be.equal();
	});

});