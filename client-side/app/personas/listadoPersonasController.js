// Utilizamos esta construccion rara para evitar
// corromper el namespace global con variables 
// que solo se utilizan para esta definición

(function(angular) {

	// con esto solicitamos una referencia al modulo previamente definido
	var app = angular.module('personas');

	// con esto definimos el contorlador y asignamos funciones
	app.controller('listadoPersonasCtrl',["$scope","servicioPersonas", function($scope,servicioPersonas){
	   servicioPersonas.getPersonas()
        .then(function(data){
            $scope.people = data;
        });	    
	}]);

})(angular);