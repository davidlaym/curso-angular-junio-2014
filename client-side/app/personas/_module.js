// Recomiendo siempre separar la definición del módulo en un archivo aparte
// ya que cuando se hace más complejo, se configuran varios parámetros
// y dependencias, que no debieran estar junto con controllers,
// servicios ni directivas


// al colocar [] al final, indicamos que no tiene dependencias.
// si no colocaramos el [], estaríamos solicitando un módulo previamente
// definido y daría un error porque no lo encontraría.
angular
	.module('personas',['ngResource', 'ngRoute'])

    .config(["$routeProvider", function($routeProvider){
        $routeProvider
            .when("/person/:personId", {
                templateUrl: 'detallePersonas.html',
                controller: 'detallePersonasCtrl'
            }) 
            .otherwise({
                templateUrl: "listadoPersonas.html",
                controller: 'listadoPersonasCtrl'
            })
    }]);

