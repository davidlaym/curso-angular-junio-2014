
var express = require('express');
var app = module.exports = express();

var http = require('http');

var config = require('./config/express_config.js')(app, express);

//var cp = require('child_process');
//
//var grunt = cp.spawn(process.env.comspec, ['/c','grunt','dev'])
//
//grunt.on('error', function(err) {
//    // relay output to console
//    console.log("%s", err.stack)
//});
//
//grunt.stdout.on('data', function(data) {
//    // relay output to console
//    console.log("%s", data)
//});
//
//
//grunt.stderr.on('data', function(data) {
//    // relay output to console
//    console.error("%s", data)
//});
http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});